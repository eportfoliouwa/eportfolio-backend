import os

class Config(object):
	SECRET_KEY = os.environ.get("FLASK_SECRET", "8QZSRz56ggCQggdVqTFDZj5g5aZHdnsPUCBSSgcrWJ")

	# Enable protection agains *Cross-site Request Forgery (CSRF)*
	CSRF_ENABLED     = True

	# Use a secure, unique and absolutely secret key for
	# signing the data.
	CSRF_SESSION_KEY = os.environ.get("CSRF_SESSION_KEY", "RdMtH9MmvdBkvRsYFN5JWnsUvhbdJuMpktmPBQZ8vJ")


	MAX_CONTENT_LENGTH = 5 * 1024 * 1024 #5MB Limit

	# CONSUMER_KEY_PEM_FILE = os.path.abspath('consumer_key.pem')
	# with open(CONSUMER_KEY_PEM_FILE, 'w') as wfile:
	#     wfile.write(os.environ.get('CONSUMER_KEY_CERT', ''))

	PYLTI_CONFIG = {
	    "consumers": {
	        "UWA_LMS_EPORTFOLIO": {
	            "secret": os.environ.get("CONSUMER_KEY_SECRET", "3sCeLrYpJRDiVfWW1RDKpTG4Vx2ERRHGM3YmybADUZ4L3Q0udMiwxKBpOAVvlg3"),
	            "cert": None
	            #"cert": CONSUMER_KEY_PEM_FILE
	        }
	    }
	}

	SESSION_COOKIE_DOMAIN = ".engportfol.io"

	ALLOWED_EXTENSIONS = ["pdf", "doc", "docx", "rtf"]

	AWS_S3_BUCKET_NAME = "eng-portfolio"
	AWS_API_ACCESS_KEY = "AKIAIJEF5GZTRPMNANQQ"
	AWS_API_SECRET_KEY = "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"

# Local Development
# Disables LTI and ACL Checks
class LocalDevelopmentConfig(Config):
	
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(os.path.abspath(os.path.dirname(__file__)), 'development.db')
	DATABASE_CONNECT_OPTIONS = {}
	SQLALCHEMY_TRACK_MODIFICATIONS = False

	SQLALCHEMY_SESSION_OPTIONS = {"autoflush": True}

	DEBUG = True
	TESTING = True

	EPORTFOLIO_FRONTEND_URL = "https://www.test.engportfol.io/student/#/home/"

	ORIGINS = ["https://www.engportfol.io", "https://www.test.engportfol.io", "http://localhost", "http://localhost:8000"]

# Cirlce CI Testing
# Disables LTI and ACL checks
class TestingConfig(Config):
	DEBUG = True
	TESTING = True

	# database setup for CircleCI
	SQLALCHEMY_DATABASE_URI = "mysql://ubuntu@localhost/circle_test"
	SQLALCHEMY_SESSION_OPTIONS = {}
	SQLALCHEMY_TRACK_MODIFICATIONS = False

	EPORTFOLIO_FRONTEND_URL = "https://www.test.engportfol.io/student/#/home/"

	ORIGINS = ["https://www.engportfol.io"]

# AWS Deployed "test-api.engportfol.io"
# Diables LTI and ACL checks
class DeployedTestingConfig(Config):
	DEBUG = True
	TESTING = True

	SQLALCHEMY_DATABASE_URI = "mysql://eportfolio_test:gMBqDzJj4mW5@localhost/eportfolio_test"
	SQLALCHEMY_SESSION_OPTIONS = {}
	SQLALCHEMY_TRACK_MODIFICATIONS = False

	EPORTFOLIO_FRONTEND_URL = "https://www.test.engportfol.io/student/#/home/"

	ORIGINS = ["https://www.engportfol.io", "https://www.test.engportfol.io", "http://localhost", "http://localhost:8000"]

# AWS Deployed "api.engportfol.io"
# Enables LTI and ACL checks
class DeployedStagingConfig(Config):

	DEBUG = True
	TESTING = False

	SQLALCHEMY_DATABASE_URI = "mysql://eportfolio_stage:uqCJf2J4rNSK@localhost/eportfolio_stage"
	SQLALCHEMY_SESSION_OPTIONS = {}
	SQLALCHEMY_TRACK_MODIFICATIONS = False

	EPORTFOLIO_FRONTEND_URL = "https://www.engportfol.io/student/#/home/"

	ORIGINS = ["https://www.engportfol.io"]



