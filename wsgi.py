import os

def application(environ, start_response):
    os.environ['EPORTFOLIO_MODE'] = environ['EPORTFOLIO_MODE']
    from app import app as _application
    return _application(environ, start_response)

if __name__ == '__main__':
    _application.run(debug=False, threaded=True)