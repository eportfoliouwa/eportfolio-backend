-- MySQL dump 10.13  Distrib 5.5.52, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eportfolio_stage
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `competency`
--

DROP TABLE IF EXISTS `competency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competency` (
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(12) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `description` text,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competency`
--

LOCK TABLES `competency` WRITE;
/*!40000 ALTER TABLE `competency` DISABLE KEYS */;
INSERT INTO `competency` VALUES ('2016-10-31 15:34:35','2016-10-31 15:34:35',1,'1.1','Comprehensive, theory based understanding','Comprehensive, theory based understanding of the underpinning natural and physical sciences and the engineering fundamentals applicable to the engineering discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:36','2016-10-31 15:34:36',2,'1.2','Conceptual understanding','Conceptual understanding of the, mathematics, numerical analysis, statistics, and computer and information sciences which underpin the engineering discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:36','2016-10-31 15:34:36',3,'1.3','In-depth understanding','In-depth understanding of specialist bodies of knowledge within the engineering discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:37','2016-10-31 15:34:37',4,'1.4','Discernment','Discernment of knowledge development and research directions within the engineering discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:38','2016-10-31 15:34:38',5,'1.5','Knowledge','Knowledge of contextual factors impacting the engineering discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:38','2016-10-31 15:34:38',6,'1.6','Understanding','Understanding of the scope, principles, norms, accountabilities and bounds of contemporary engineering practice in the specific discipline.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:39','2016-10-31 15:34:39',7,'2.1','Engineering Methods','Application of established engineering methods to complex engineering problem solving.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:40','2016-10-31 15:34:40',8,'2.2','Techniques, tools and resources','Fluent application of engineering techniques, tools and resources.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:40','2016-10-31 15:34:40',9,'2.3','Synthesis and deisgn','Application of systematic engineering synthesis and design processes.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:41','2016-10-31 15:34:41',10,'2.4','Management of engineering projects','Application of systematic approaches to the conduct and management of engineering projects.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:42','2016-10-31 15:34:42',11,'3.1','Ethical conduct','Ethical conduct and professional accountability.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:43','2016-10-31 15:34:43',12,'3.2','Effective communication','Effective oral and written communication in professional and lay domains.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:43','2016-10-31 15:34:43',13,'3.3','Creative, innovative and pro-active demeanour.','Creative, innovative and pro-active demeanour.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:44','2016-10-31 15:34:44',14,'3.4','Professional use and management of information','Professional use and management of information.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:44','2016-10-31 15:34:44',15,'3.5','Comprehensive, theory based understanding','Orderly management of self, and professional conduct.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards'),('2016-10-31 15:34:45','2016-10-31 15:34:45',16,'3.6','Effective team membership and team leadership','Effective team membership and team leadership.','https://www.engineersaustralia.org.au/about-us/program-accreditation#standards');
/*!40000 ALTER TABLE `competency` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-03  7:50:57
