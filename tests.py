import app.tests.end_to_end.ete_tests as ete_tests
import app.tests.models.model_tests as model_tests
import app.tests.schemas.schema_tests as schema_tests

import sys

SUITES = ["ete", "models", "schemas"]

def usage(error):
	if error == "arglen":
		print "Usage: python tests.py <suite>"

	elif error == "suite":
		print "Suites: ", SUITES

'''
TODO configurable opts to decide which tests to run
'''
def main():
	if len(sys.argv) is not 2:
		usage("arglen")
		raise ValueError("Invalid length of args.")

	suite = sys.argv[1]

	if suite not in SUITES:
		usage("suite")
		raise ValueError("Invalid test suite specified")

	if suite == "ete":
		ete_tests.run_tests()
	elif suite == "models":
		model_tests.run_tests()
	elif suite == "schemas":
		schema_tests.run_tests()

if __name__ == '__main__':
	main()