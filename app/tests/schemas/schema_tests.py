import unittest
import os
import inspect
import xmlrunner

from app.tests.tools.dict_debugger 				import DictDebugger
from app.tests.schemas.base_schema_test_case 	import BaseSchemaTestCase

from app.eportfolio.schema 						import *
from app.eportfolio.models 						import *

PWD = os.path.dirname(__file__)

'''
Put this in every test suite
Resolve a test to a directory containing its files

resulting test path = PWD + name_of_test_method
'''
def get_path():
	return PWD + "/" + inspect.stack()[1][3] + "/" 

'''
SERIALIZATION test flow:
	1. Construct an instance of one of the models (i.e. a python object)
	2. Apply the dump functionality of a schema to the python object
	3. Assert that the JSON returned by the schema's dump process matches the expected.json file.

DESERIALIZATION test flow:
	1. Interpret the JSON file to be deserialized, to create a dictionary that reflects how the 
		resulting python objects __dict__() function should look after deserialization.
	2. Apply the load functionality of a schema to the JSON file.
	3. Assert that the object's __dict__() returns the same dictionary as what was constructed in step (1).

'''
class SchemaTestCase(BaseSchemaTestCase):

	def setUp(self):
		super(SchemaTestCase, self).setUp()

	def tearDown(self):
		super(SchemaTestCase, self).tearDown()

	###################
	''' EntrySchema ''' 
	###################

	@unittest.skip("Unimplemented")
	def test_serialize_entry(self):
		
		'''1. Construct an instance of one of the models (i.e. a python object)'''

		# test_path 			= get_path()
		# input_json			= self.json(test_path, "input")

		# app = self.create_app(self)		

		# kwargs = 	{
		# 				"name": "Having fun",
		# 				"number": 1, 
		# 				"start_date": None,
		# 				"end_date": None, 
		# 				"description": None, 
		# 				"entry_type": "Category 1",
		# 				"hours_accrued": 0, 
		# 				"supervisor_name": "", 
		# 				"supervisor_position": "", 
		# 				#relationships
		# 				"episodes": [],
		# 				"attachments": [],
		# 				"feedback": []
		# 			}
		
		# entry = Entry(**kwargs)

		'''2. Apply the dump functionality of a schema to the python object'''

		# data = EntrySchema().dump(input_json["data"])
		
		'''3. Assert that the JSON returned by the schema's dump process matches the expected.json file.'''
		pass
	
	# @unittest.skip("staging")
	def test_deserialize_entry(self):
		#1. Interpret the JSON file to be deserialized, to create a dictionary that reflects how the 
		#resulting python objects __dict__() function should look after deserialization.
		test_path 			= get_path()
		input_json			= self.json(test_path, "input")

		#2. Apply the load functionality of a schema to the JSON file.

		entry_object, errors = EntrySchema().load(input_json["data"])

		if errors:
			print errors
			raise ValueError("Marshmallow errors: ", str(errors))

		# print "the object has these:\n"
		# print entry_object.__dict__

		#3. Assert that the object's __dict__() returns the same dictionary as what was constructed in step (1).
		expected_dict 	 	= self.expected(test_path)
		
		self.assertDeserializationSuccess(expected_dict, entry_object, verbosity=2)
	
def run_tests(implemented_only=True):
	suite = unittest.TestLoader().loadTestsFromTestCase(SchemaTestCase)
	unittest.runner = xmlrunner.XMLTestRunner(output=os.environ['CIRCLE_TEST_REPORTS'] + "/junit/test-results/")
	result = unittest.runner.run(suite)
	if result.wasSuccessful():
		exit()
	else:
		exit(1)

if __name__ == '__main__':
	run_tests()