import json
import sys
import os

from flask_sqlalchemy 				import SQLAlchemy
from flask_testing 					import TestCase, LiveServerTestCase

from app 							import app, db
from app.tests.tools.dict_debugger 	import DictDebugger
from app.tests.tools.obj_to_json 	import obj_to_json

from ims_lti_py 					import ToolConsumer, ToolConfig
import urllib
import urllib2

from werkzeug.http 					import parse_cookie

VERSION	= "/v1"
PWD 	= os.path.dirname(__file__)

REST_VERBOSE = False

'''
Implements functionality for asserting upon the result of serialization 
	i.e. comparing the resulting JSON and supplied expected JSON

Implements functionality for asserting upon the result of deserialization
	i.e. comparing the __dict__() of the resulting object to an expected dictionary.
'''
class BaseSchemaTestCase(TestCase):
	PRESERVE_CONTEXT_ON_EXCEPTION 	= False
	SQLALCHEMY_DATABASE_URI 		= "sqlite://"
	TESTING 						= True

	def create_app(self):
		return app

	def setUp(self):
		db.create_all()

	def tearDown(self):
		db.session.remove()
		db.drop_all()

	def json(self, test_path, filename):
		with open(test_path+filename+".json") as json_file:
			return json.load(json_file)
	
	def expected(self, test_path):
		return self.json(test_path, filename="expected")

	'''
	Implements functionality for asserting upon the result of serialization 
	i.e. comparing the resulting JSON and supplied expected JSON
	'''
	def assertSerializationSuccess(self, expected, actual, verbosity=1):
		return self.assertJsonEqual(expected, actual, verbosity)
	
	'''
	Implements functionality for asserting upon the result of deserialization
	i.e. comparing the __dict__() of the resulting object to an expected dictionary.
	'''
	def assertDeserializationSuccess(self, expected, _object, verbosity=1):
		actual = obj_to_json(_object)
		return self.assertJsonEqual(expected, actual, verbosity)

	def assertJsonEqual(self, expected, actual, verbosity=1):
		dd = DictDebugger(expected=expected, actual=actual)
		error_msg = None
		
		if verbosity > 0:
			error_msg = '\n' + dd.report()

			if verbosity is 2:
				error_msg += '\n\n' + dd.print_both()

		self.assertTrue(dd.equal(), msg=error_msg)


	