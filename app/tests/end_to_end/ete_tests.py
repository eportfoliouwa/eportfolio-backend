import unittest
import os
import inspect
import xmlrunner

from app.tests.tools.dict_debugger 				import DictDebugger
from app.tests.end_to_end.base_ete_test_case 	import BaseEteTestCase

PWD = os.path.dirname(__file__)

'''
Put this in every test suite
Resolve a test to a directory containing its files

resulting test path = PWD + name_of_test_method
'''
def get_path():
	return PWD + "/" + inspect.stack()[1][3] + "/" 

class EteTestCase(BaseEteTestCase):

	def setUp(self):
		super(EteTestCase, self).setUp()
		#extra stuff (possibly auth related)

	def tearDown(self):
		super(EteTestCase, self).tearDown()
		#extra stuff

	'''
	Category: View
	'''

	'''
	Basic portfolio elements
	'''
	#@unittest.skip("staging")
	def test_get_empty_portfolio(self):
		test_path 			= get_path()
		url 				= "/eportfolios/534"
		response 			= self.get(url)
		expected_response 	= self.expected(test_path)
		
		#get the portfolio
		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=2)

	#@unittest.skip("staging")
	def test_get_portfolio_with_entries(self):
		test_path 			= get_path()

		portfolio_url 		= "/eportfolios/534"
		entries_url 		= portfolio_url + "/entries"

		industry_entry_json = self.json(test_path, "industry_entry")
		prof_entry_json 	= self.json(test_path, "prof_devmt_entry")
		other_entry_json 	= self.json(test_path, "other_entry")

		#post 'industry' entry to student 534
		self.post(entries_url, industry_entry_json)

		#post 'professional experience' entry to student 534
		self.post(entries_url, prof_entry_json)

		#post 'other' entry to student 534
		self.post(entries_url, other_entry_json)



		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=1)

	# #@unittest.skip("staging")
	def test_get_portfolio_with_episodes(self):
		test_path 			= get_path()
		portfolio_url 		= "/eportfolios/534"

		#post entries to portfolio
		entries_url 		= portfolio_url + "/entries"

		industry_entry_json = self.json(test_path, "industry_entry")
		prof_entry_json 	= self.json(test_path, "prof_devmt_entry")

		self.assertResponse(self.post(entries_url, industry_entry_json), 	201)
		self.assertResponse(self.post(entries_url, prof_entry_json), 		201)

		industry_episodes_url 	= entries_url + "/1/episodes"
		prof_episodes_url 		= entries_url + "/2/episodes"

		episode_1_json		= self.json(test_path, "episode1")
		episode_2_json 		= self.json(test_path, "episode2")
		episode_3_json 		= self.json(test_path, "episode3")

		#post episodes to respective entries
		self.assertResponse(self.post(industry_episodes_url, 	episode_1_json), 201)
		self.assertResponse(self.post(industry_episodes_url, 	episode_2_json), 201)
		self.assertResponse(self.post(prof_episodes_url, 		episode_3_json), 201)

		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=2)

	@unittest.skip("non-deterministic test; unexpectedly fails")
	def test_get_portfolio_with_competencies(self):
		test_path 			= get_path()
		portfolio_url 		= "/eportfolios/534"
		
		#post entries to portfolio
		entries_url 		= portfolio_url + "/entries"
		
		industry_entry_json = self.json(test_path, "industry_entry")
		prof_entry_json 	= self.json(test_path, "prof_devmt_entry")
		
		self.post(entries_url, industry_entry_json)
		self.post(entries_url, prof_entry_json)

		#post episodes to respective entries
		industry_episodes_url 	= entries_url + "/1/episodes"
		prof_episodes_url 		= entries_url + "/2/episodes"

		episode_1_json		= self.json(test_path, "episode1")
		episode_2_json 		= self.json(test_path, "episode2")
		episode_3_json 		= self.json(test_path, "episode3")

		self.post(industry_episodes_url, 	episode_1_json)
		self.post(industry_episodes_url, 	episode_2_json)
		self.post(prof_episodes_url, 		episode_3_json)

		#add competencies to episodes
		episode_1_url = industry_episodes_url 	+ "/1"
		episode_3_url = prof_episodes_url 		+ "/1"

		episode_1_put_json	= self.json(test_path, "episode1_comp")
		episode_3_put_json 	= self.json(test_path, "episode3_comp")

		self.put(episode_1_url, episode_1_put_json)
		self.put(episode_3_url, episode_3_put_json)

		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=2)

	@unittest.skip("unimplemented view")
	def test_get_portfolio_with_feedback(self):
		test_path 			= get_path()
		portfolio_url 		= "/eportfolios/534"

		#create a marker
		markers_url 		= "/markers"
		marker_json 		= self.json(test_path, "marker")

		self.post(markers_url, marker_json)
		
		#add (portfolio-level) feedback to portfolio
		
		feedback_url 		= portfolio_url + "/feedback"
		feedback_json 		= self.json(test_path, "feedback")

		self.post(feedback_url, feedback_json)

		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=1)

	def test_lti_cookie_present(self):
		test_path 			= get_path()
		url 				= "/lti"
		response 			= self.setupLTISession()

		self.assertResponse(response, 302)
		self.assertLTICookiesPresent(response)
		

	'''
	Portfolio submission states
	'''
	@unittest.skip("unimplemented")
	def test_get_submitted_portfolio(self):
		#submit portfolio
		
		#get the portfolio
		
		pass

	@unittest.skip("unimplemented")
	def test_get_rejected_portfolio(self):
		#create marker
		
		#assign student to marker
		
		#submit portfolio
		
		#reject a portfolio
		
		#get the portfolio
		
		pass

	@unittest.skip("unimplemented")
	def test_get_passed_portfolio(self):
		#create marker
		
		#assign student to marker
		
		#submit portfolio
		
		#pass portfolio
		
		#get the portfolio
		
		pass	

	'''
	Portfolio status
	'''

	@unittest.skip("Needs revision due to back-end changes")
	def test_get_portfolio_with_resume(self):
		test_path 			= get_path()
		portfolio_url 		= "/eportfolios/534"

		#create attachment of type resume
		attachments_url 	= portfolio_url + "/attachments"
		resume_json 		= self.json(test_path, "resume")

		self.post(attachments_url, resume_json)

		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=1)

	#@unittest.skip("staging")
	def test_get_portfolio_with_satisfied_hours(self):
		test_path 			= get_path()

		portfolio_url 		= "/eportfolios/534"
		entries_url 		= portfolio_url + "/entries"

		industry_entry_json = self.json(test_path, "industry_entry") 	#450 hours
		prof_entry_json 	= self.json(test_path, "prof_devmt_entry")	#20 hours
		
		#post entries
		self.assertResponse(self.post(entries_url, industry_entry_json), 	201)
		self.assertResponse(self.post(entries_url, prof_entry_json), 		201)
		
		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=1)

	#@unittest.skip("staging")
	def test_get_portfolio_with_satisfied_category_2_only(self):
		#i.e. portfolio with 200 hours in category 2
		#test verifies that only 150 of these hours are passed on
		test_path 			= get_path()

		portfolio_url 		= "/eportfolios/534"
		entries_url 		= portfolio_url + "/entries"

		industry_entry_json = self.json(test_path, "industry_entry") 	#250 hours
		prof_entry_json 	= self.json(test_path, "prof_devmt_entry")	#100 hours
		other_entry_json 	= self.json(test_path, "other_entry")		#100 hours

		#post entries
		self.post(entries_url, industry_entry_json)
		self.post(entries_url, prof_entry_json)
		self.post(entries_url, other_entry_json)

		#get the portfolio
		response 			= self.get(portfolio_url)
		expected_response 	= self.expected(test_path)

		self.assert200(response)
		self.assertJsonEqual(expected_response, response.json, verbosity=1)

	@unittest.skip("unimplemented")
	def test_get_portfolio_with_satisfied_competencies(self):
		#portfolio with 16/16 competencies, demonstrated across 
		#the SELECTED episodes
		pass

	@unittest.skip("unimplemented")
	def test_get_portfolio_with_satisfied_career_episodes(self):
		pass

	@unittest.skip("unimplemented")
	def test_get_portfolio_valid_for_submission(self):
		pass

	'''
	Category: Change
	'''
	@unittest.skip("unimplemented")
	def test_mark_portfolio_pass(self):
		pass

	@unittest.skip("unimplemented")
	def test_submit_portfolio(self):
		pass

	@unittest.skip("unimplemented")
	def test_submit_incompelete_portfolio(self):
		pass

	'''
	Negative tests
	'''
	@unittest.skip("unimplemented")
	def test_student_404(self):
		#get portfolio of non-existent student
		#404
		pass

	'''
	TODO - Auth

	(When auth is implemented)

	Maybe make auth a separate test case
	'''
	@unittest.skip("unimplemented")
	def test_get_other_student_portfolio(self):
		#assert 403 forbidden
		pass
	
def run_tests(implemented_only=True):
	suite = unittest.TestLoader().loadTestsFromTestCase(EteTestCase)
	unittest.runner = xmlrunner.XMLTestRunner(output=os.environ['CIRCLE_TEST_REPORTS'] + "/junit/test-results/")
	result = unittest.runner.run(suite)

	if result.wasSuccessful():
		exit()
	else:
		exit(1)

if __name__ == '__main__':
	run_tests()