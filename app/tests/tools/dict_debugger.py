import pprint
import collections

'''
http://stackoverflow.com/questions/6027558/
flatten-nested-python-dictionaries-compressing-keys
'''
def flatten(d, parent_key='', sep='.'):
	items = []
	for k, v in d.items():
		new_key = parent_key + sep + k if parent_key else k
		if isinstance(v, collections.MutableMapping):
			items.extend(flatten(v, new_key, sep=sep).items())

		if isinstance(v, list):
			for i in range(len(v)):
				_key = new_key + "%d"%(i+1)
				items.extend(flatten(v[i], _key, sep=sep).items())
		
		elif not isinstance(v, dict): # to stop it appending the old dict.
			items.append((new_key, v))
	
	return dict(items)


'''
http://stackoverflow.com/questions/1165352/
calculate-difference-in-keys-contained-in-two-python-dictionaries
'''
class DictDebugger(object):
	"""
	Calculate the difference between two dictionaries as:
	(1) items added
	(2) items removed
	(3) keys same in both but changed values
	(4) keys same in both and unchanged values
	"""
	def __init__(self, expected={}, actual={}):
		if "data" in expected.keys() and "data" in actual.keys():
			expected, actual = expected["data"], actual["data"]

		expected 	= flatten(expected)
		actual 		= flatten(actual)

		#apply wildcards
		for k, v in expected.items():
			if v == "*":
				if k in actual.keys():
					actual[k] = unicode("*")

		self.expected, self.actual = expected, actual
		self.set_current, self.set_past = set(actual.keys()), set(expected.keys())
		self.intersect = self.set_current.intersection(self.set_past)

	def added(self):
		return self.set_current - self.intersect 
	
	def removed(self):
		return self.set_past - self.intersect 
	
	def changed(self):
		return set(o for o in self.intersect if self.actual[o] != self.expected[o])
	
	def unchanged(self):
		return set(o for o in self.intersect if self.actual[o] == self.expected[o])	

	def equal(self):
		return not (0 < len(self.added()) + len(self.removed()) + len(self.changed()))

	def report(self):
		output = '\n' + "Added:" +  str(self.added()) + "\n\nRemoved:" +  str(self.removed())  + "\n\nChanged:" +  str(self.changed()) + '\n'
		
		if self.changed():
			for key in self.changed():
				output += "\t%s\n\t\tExpected:%s\n\t\tActual:%s\n" % (key, self.expected[key], self.actual[key])
			

		return output

	def print_both(self):
		output = ("---Expected---"  + '\n' +
				  pprint.pformat(self.expected, indent=2)  + '\n' +
				  "---actual---"  + '\n' +
				  pprint.pformat(self.actual, indent=2)  + '\n')

		return output


	def print_actual(self):
		return (pprint.pformat(self.actual, indent=2)  + '\n')