import json



def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable")


def obj_to_json(obj):
	e_dict = obj.__dict__
	del e_dict['_sa_instance_state']

	return e_dict