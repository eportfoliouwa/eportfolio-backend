import json
import sys
import os

from flask_sqlalchemy 				import SQLAlchemy
from flask_testing 					import TestCase, LiveServerTestCase

from app 							import app, db
from app.tests.tools.dict_debugger 	import DictDebugger

from ims_lti_py import ToolConsumer, ToolConfig
import urllib
import urllib2

from werkzeug.http import parse_cookie

VERSION	= "/v1"
PWD 	= os.path.dirname(__file__)

REST_VERBOSE = False

LTI_CLIENT_KEY = 'UWA_LMS_EPORTFOLIO'
LTI_CLIENT_SECRET = '3sCeLrYpJRDiVfWW1RDKpTG4Vx2ERRHGM3YmybADUZ4L3Q0udMiwxKBpOAVvlg3'

'''
For testing endpoints based on http responses
and database assertions
'''
class BaseModelTestCase(TestCase):
	PRESERVE_CONTEXT_ON_EXCEPTION 	= False
	SQLALCHEMY_DATABASE_URI 		= "sqlite://"
	TESTING 						= True

	def create_app(self):
		return app

	def setUp(self):
		db.create_all()
		response =	self.client.get("/test_setup")

	def tearDown(self):
		db.session.remove()
		db.drop_all()