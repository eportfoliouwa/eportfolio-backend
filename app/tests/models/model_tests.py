import unittest
import os
import inspect
import xmlrunner

from app.tests.tools.dict_debugger 			import DictDebugger
from app.tests.models.base_model_test_case 	import BaseModelTestCase

from app.eportfolio.models 					import *

import datetime

PWD = os.path.dirname(__file__)

'''
Put this in every test suite
Resolve a test to a directory containing its files

resulting test path = PWD + name_of_test_method
'''
def get_path():
	return PWD + "/" + inspect.stack()[1][3] + "/" 

'''
Initialisation test case
	Create an instance of a model by invoking the model's constructor.
	Assert that the attributes of the instantiated object are consistent with expectations.
	Assert that methods associated with the object that involve calculation return correct values.


Mutation test case
	Load an instance of a model from the database by performing a query on test data (created by the setup method of the BaseModelTestCase class).
	Assert that the attributes of the mutated object are consistent with expectations.
	Assert that methods associated with the object that involve calculation return correct values.

'''
class ModelTestCase(BaseModelTestCase):

	def setUp(self):
		super(BaseModelTestCase, self).setUp()
		#extra stuff (possibly auth related)

	def tearDown(self):
		super(BaseModelTestCase, self).tearDown()
		#extra stuff

	#############
	'''ENTRIES'''
	#############

	@unittest.skip("unimplemented")
	def test_initialise_entry(self):
		test_path 			= get_path()
		# app 				= self.create_app()

		#Create an instance of a model by invoking the model's constructor.
		kwargs = 	{
						"name": "Having fun",
						"number": 1, 
						"start_date": datetime.date(2012, 04, 23), 	
						"end_date": datetime.date(2012, 05, 13), 
						"description": "writing design documents was lots of fun!", 
						"entry_type": "Category 1",
						"hours_accrued": 230, 
						"supervisor_name": "Mr Burns", 
						"supervisor_position": "Rich",
					}


		entry = Entry(**kwargs)

		db.session.add(entry)
		db.session.commit()

		# e1 = Entry.query.get_or_404(1)
		# print e1.__dict__

		# exit()

		#Assert that the attributes of the instantiated object are consistent with expectations.
		print entry
		print entry.id

		print type(entry)

		assert entry.id 					== 1
		assert entry.name				 	== "Having fun"
		assert entry.number				 	== 1 
		assert entry.start_date			 	== datetime.date(2012, 04, 23)
		assert entry.end_date		 	 	== datetime.date(2012, 05, 13)
		assert entry.description		 	== "writing design documents was lots of fun!" 
		assert entry.entry_type			 	== "Category 1"
		assert entry.hours_accrued		 	== 230 
		assert entry.supervisor_name	 	== "Mr Burns" 
		assert entry.supervisor_position 	== "Rich" 
		assert len(entry.attachments)		 == 0
		assert len(entry.feedback	)		 	== 0

		#Assert that methods associated with the object that involve calculation return correct values.

		assert entry.get_url() == '/v1/eportfolios/534/entries/1'
		assert not entry.competency_set()

		

	@unittest.skip("unimplemented")
	def test_mutate_entry(self):

		#Load an instance of a model from the database by performing a query on test data (created by the setup method of the BaseModelTestCase class).
		
		e1 				= Entry.query.get_or_404(1)
		
		#Assert that the attributes of the mutated object are consistent with expectations.

		''' trivial case: changing attributes directly '''
		
		e1.description 	= "writing design documents really lots of fun!"


		assert e1.description == "writing design documents really lots of fun!"
		
		''' 
		non-trivial case: adding episode via add_episode functionality 
		and observing the effect on the episode list 
		'''
		assert len(e1.episodes) == 0

		ep_kwargs = {
					"number": 1,
					"report": "REPORT!",
					"selected": False,
					"name": BH
					}
		

		episode = Episode(ep_kwargs)

		e1.add_episode(episode)

		assert len(e1.episodes) == 1

		#Assert that methods associated with the object that involve calculation return correct values.

		''' 
		Mutate competencies and observe effect on competency set
		Testing methods entirely
			competency added through various functionality
			presence of competency queried through set functionality
		''' 

		episode.selected == True

		competency1 = Competency.query.get_or_404(1)

		e1.add_competency(competency1)

		assert competency1 in entry.competency_set()		



def run_tests(implemented_only=True):
	suite = unittest.TestLoader().loadTestsFromTestCase(ModelTestCase)
	unittest.runner = xmlrunner.XMLTestRunner(output=os.environ['CIRCLE_TEST_REPORTS'] + "/junit/test-results/")
	result = unittest.runner.run(suite)
	if result.wasSuccessful():
		exit()
	else:
		exit(1)

if __name__ == '__main__':
	run_tests()