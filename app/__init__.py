# Import flask and template operators
from flask import Flask, render_template

# Import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy

# Import Flask-Marshmallow
from flask_marshmallow import Marshmallow

# Import Flask-Cors for Cross-Site Origin validation
from flask_cors import CORS, cross_origin

# Needed to access ENVVARs
import os

# Access UWSGI Environment
from werkzeug.wrappers import Request

# Define the WSGI application object
app = Flask(__name__)

# Configurations
try:
	mode = os.environ['EPORTFOLIO_MODE'] # if that fails, try the local OS as we must be running outside WSGI.
except:
	mode = None
	app.logger.error("No config loaded!")


if mode == "DEV":
	app.config.from_object('config.LocalDevelopmentConfig')
	app.logger.info('Loading Local Development Configuration...')
	app.logger.info('DB Engine URL: ' + app.config['SQLALCHEMY_DATABASE_URI'])
elif mode == "TEST":
	app.config.from_object('config.TestingConfig')
	app.logger.info('Loading Testing Configuration...')
	app.logger.info('DB Engine URL: ' + app.config['SQLALCHEMY_DATABASE_URI'])
elif mode == "DEPLOYED_TESTING":
	app.config.from_object('config.DeployedTestingConfig')
	app.logger.info('Loading Deployed Testing Configuration...')
	app.logger.info('DB Engine URL: ' + app.config['SQLALCHEMY_DATABASE_URI'])
elif mode == "DEPLOYED_STAGING":
	app.config.from_object('config.DeployedStagingConfig')
	app.logger.info('Loading Deplpoyed Staging Configuration...')
	app.logger.info('DB Engine URL: ' + app.config['SQLALCHEMY_DATABASE_URI'])

# Allow the frontend page to make calls to the backend
allowed_headers = ["Location", "Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Content-Length"]
exposed_headers = allowed_headers

cors = CORS(app, origins=app.config["ORIGINS"], supports_credentials=True, expose_headers=exposed_headers, allow_headers=allowed_headers)

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app, session_options=app.config["SQLALCHEMY_SESSION_OPTIONS"])

# Init the Marshmallow object.
ma = Marshmallow(app)

# Import a module / component using its blueprint handler variable (mod_auth)
from app.eportfolio.views import eportfolio as eportfolio_blueprint

# Register blueprint(s)
app.register_blueprint(eportfolio_blueprint)

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()
