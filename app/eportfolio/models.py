from app import app, db
from flask import current_app, url_for

from sqlalchemy.dialects import mysql
from bs4 import BeautifulSoup as bs
from collections import defaultdict
from datetime import datetime


'''
For managing many-to-many of attachments at the entry level
'''
entry_attachments = db.Table(
	'entry_attachments',
	db.Column('attachment_id', db.Integer, db.ForeignKey('attachment.id')),
	db.Column('entry_id', db.Integer, db.ForeignKey('entry.id'))
)

'''
For managing many-to-many of attachments at the CeReport level
'''
career_episode_attachments = db.Table(
	'career_episode_attachments',
	db.Column('attachment_id', db.Integer, db.ForeignKey('attachment.id')),
	db.Column('career_episode_id', db.Integer, db.ForeignKey('career_episode.id'))
)

career_episode_competencies = db.Table(
	'career_episode_competencies',
	db.Column('competency_id', db.Integer, db.ForeignKey('competency.id')),
	db.Column('career_episode_id', db.Integer, db.ForeignKey('career_episode.id'))
)


'''
Base Class to provide some useful model defaults
'''
class Base(db.Model):
	__abstract__  = True

	date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
										   onupdate=db.func.current_timestamp())

class User(Base):
	#pk
	id 			= db.Column(db.Integer, primary_key=True)
	
	#data
	first_name 	= db.Column(db.String(50))
	last_name 	= db.Column(db.String(50))
	user_email 	= db.Column(db.String(120))	
	

	#inheritance handling
	role 			= db.Column(db.String(50))
	__mapper_args__ = {'polymorphic_on': role}

	def __init__(self, id, first_name=None, last_name=None, user_email=None):
		self.id 		= id
		self.first_name = first_name
		self.last_name 	= last_name
		self.user_email = user_email

	def __repr__(self):
		return '<User %r>' % (self.first_name+" "+self.last_name)

class Marker(User):
	#pk
	id 			= db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
	
	#relationships
	students 	= db.relationship('Student', foreign_keys="Student.marker_id", backref='marker')

	#inheritance handling
	__mapper_args__ = {
		'polymorphic_identity':'marker',
		'inherit_condition': (id == User.id)
	}

	def __init__(self, id, first_name=None, last_name=None, user_email=None, students=[]):
		super(Marker, self).__init__(id, first_name, last_name, user_email)

		self.students = students

	def add_students(self, students):
		if isinstance(students, list):
			self.students += [students]
		else:
			self.students.append(students)

class UnitAdmin(User):
	#pk
	id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)

	#inheritance handling
	__mapper_args__ = {
		'polymorphic_identity':'unit_admin',
		'inherit_condition': (id == User.id)
	}

	def __init__(self, id, first_name=None, last_name=None, user_email=None):
		super(UnitAdmin, self).__init__(id, first_name, last_name, user_email)

class Student(User):
	#pk
	id 			= db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
	
	#data
	marker_id	= db.Column(db.ForeignKey('marker.id'))

	#relationships
	portfolio 	= db.relationship('Portfolio', backref='student', uselist=False)	#one-to-one

	#inheritance handling
	__mapper_args__ = {
		'polymorphic_identity':'student',
		'inherit_condition': (id == User.id)
	}


	def __init__(self, id, first_name=None, last_name=None):
		super(Student, self).__init__(id, first_name, last_name)
		
		self.user_email = str(id) + "@student.uwa.edu.au"
		self.portfolio = Portfolio(id=id)

class Portfolio(Base):
	#pk
	id 				= db.Column(db.Integer, db.ForeignKey('student.id'), primary_key=True)	#portfolio id = student id

	#data
	status 			= db.Column(db.Enum('Pass','Rejected','Submitted','Not Submitted'))
	mark 			= db.Column(db.Float)
	submission_date = db.Column(db.DateTime)

	#relationships
	feedback 		= db.relationship('PortfolioFeedback', backref='portfolio')
	entries 		= db.relationship('Entry', backref='portfolio')
	attachments 	= db.relationship('Attachment', backref='portfolio')

	def __init__(self, id, status="Not Submitted", mark=None, feedback=[], entries=[], attachments=[]):
		self.id 			= id
		self.status 		= "Not Submitted"
		self.mark 			= None
		self.feedback 		= []
		self.entries 		= []
		self.attachments 	= []

	def get_url(self):
		return url_for("eportfolio.ePortfolio", version=1, student_id=self.id)

	'''
	List extensions
	'''
	def add_entries(self, entries):
		if isinstance(entries, list):
			for e in entries:
				e.number = self.get_next_entry_number()
			self.entries += entries
		else:
			entries.number = self.get_next_entry_number()
			self.entries.append(entries)

	def add_feedback(self, feedback):
		if isinstance(feedback, list):
			for fb in feedback:
				fb.__class__ = PortfolioFeedback
			self.feedback += feedback
		else:
			feedback.__class__ = PortfolioFeedback
			self.feedback.append(feedback)

	def add_attachments(self, attachments):
		if isinstance(attachments, list):
			self.attachments += attachments
		else:
			self.attachments.append(attachments)

	'''
	list ids
	'''
	def attachment_ids(self):
		return [attachment.id for attachment in self.attachments]

	def entry_ids(self):
		return [entry.id for entry in self.entries]

	def episode_ids(self):
		episode_ids = []
		for entry in self.entries:
			episode_ids.extend(entry.episode_ids())

		return episode_ids

	'''
	Hours Progress Methods
	'''
	def hours_completed_cat_1(self):
		cat_1_entries 	= filter(lambda entry: entry.is_industry(), self.entries)
		cat_1_hours 	= sum([entry.hours_accrued for entry in cat_1_entries])


		return min(cat_1_hours, 450)

	def hours_completed_cat_1_percent(self):
		cat_1_hours_completed = self.hours_completed_cat_1()
		percent = int(float(cat_1_hours_completed * 100) / 300)
		return min(percent, 100)

	def hours_completed_cat_234(self):
		cat_234_entries = filter(lambda entry:  entry.is_cat234(), self.entries)

		cat_234_hours = sum([entry.hours_accrued for entry in cat_234_entries])

		return min(150, cat_234_hours)

	def hours_completed_overall(self):
		overall_hours = self.hours_completed_cat_1() + self.hours_completed_cat_234()
		return min(450, overall_hours)

	def hours_completed_percent_overall(self):
		hours_completed = self.hours_completed_overall()
		percent = int(float(hours_completed * 100) / 450)
		return percent

	def hours_satisfied(self):
		return self.hours_completed_percent_overall() == 100

	'''
	Competencies Progress Methods
	'''
	def competency_set(self):
		comp_set = set()	#use set to manage the case where two reports exhibit same competency
		for entry in self.entries:
			comp_set = comp_set | entry.competency_set()

		return comp_set

	def competencies_completed(self):
		return len(self.competency_set())

	def competencies_completed_percent(self):
		competencies_completed = self.competencies_completed()
		return int((competencies_completed*100.0) / 16.0)

	def competencies_satisfied(self):
		return self.competencies_completed_percent() == 100

	'''
	Overall Portfolio Progress
	'''
	def resume_uploaded(self):
		try:
			for a in self.attachments:
				if a.file_type is "Resume":
					return True
		except Exception, e:
			print e
			raise e
		
		return False

	def valid_for_submission(self):
		return self.hours_satisfied() and self.competencies_satisfied() and self.resume_uploaded()

	def is_marked(self):
		try:
			return self.mark is not None
		except Exception, e:
			print e
			raise e

	def is_submitted(self):
		return self.submission_date is not None

	def number_of_entries(self):
		return len(self.entries)

	def next_entry_number(self):
		return self.number_of_entries() + 1

	'''
	User Actions
	'''
	def submit_portfolio(self):
		#set the submission date to be not None
		pass

	'''
	Accessor Methods 
	'''
	def get_entry(self, entry_number):
		# Not going to use this for now - means having to do manual 404 checks when items not found.
		entry =  filter(lambda entry: entry.number is int(entry_number), self.entries)[0]

		#TODO this method doesn't work
		print "e", entry #returns empty list (should have returned entry)
		return entry

	def get_episode(self, entry_number, episode_number):
		entry = self.get_entry(entry_number)
		return entry.get_episode(episode_number)


	'''
	Required Init helpers
	'''
	def get_next_entry_number(self):
		try:
			max_nums = max([e.number for e in self.entries])
			return max_nums + 1
		except ValueError:
			return 1	# usually means max() 

	'''
	Marker Actions
	'''
	def grade_portfolio(self):
		pass

'''
Belongs to a portfolio
Container for career epsiodes

primary_key = ()
'''
class Entry(Base):
	#PK
	id 					= db.Column(db.Integer, autoincrement=True, primary_key=True)
	student_id 			= db.Column(db.Integer, db.ForeignKey('portfolio.id'))

	#data
	name 				= db.Column(db.String(128))
	number  			= db.Column(db.Integer)
	start_date 			= db.Column(db.Date)
	end_date 			= db.Column(db.Date)
	description 		= db.Column(db.Text)
	entry_type 			= db.Column(db.Enum('Category 1','Category 2, 3 and 4','Coursework'))
	hours_accrued 		= db.Column(db.Integer)					##flag: set to zero for coursework
	supervisor_name 	= db.Column(db.String(120))
	supervisor_position = db.Column(db.String(120))

	#relationships
	feedback 			= db.relationship('EntryFeedback', backref='entry')
	attachments 		= db.relationship('Attachment', secondary='entry_attachments', backref="entry") 	#m2m
	episodes 			= db.relationship('CareerEpisode', backref='entry')

	def __init__(self, *args, **kwargs):
		#data
		self.name 					= kwargs.pop("name", None)
		self.number 				= kwargs.pop("number", None) 
		self.start_date				= kwargs.pop("start_date", None)
		self.end_date				= kwargs.pop("end_date", None) 
		self.description 			= kwargs.pop("description", None) 
		self.entry_type 			= kwargs.pop("entry_type", "Category 1") 
		self.hours_accrued 			= kwargs.pop("hours_accrued", 0) 
		self.supervisor_name		= kwargs.pop("supervisor_name", "") 
		self.supervisor_position	= kwargs.pop("supervisor_position", "") 

		#relationships
		self.episodes 				= kwargs.pop("episodes", [])
		self.attachments 			= kwargs.pop("attachments", [])
		self.feedback 				= kwargs.pop("feedback", [])

		if kwargs:
			raise TypeError("Invalid arguments: %s", kwargs.keys())

	def get_url(self):
		#print self.entry_number
		return url_for("eportfolio.entries", entry_number=self.number, version=1, student_id=self.student_id)

	'''
	List extensions
	'''
	def add_feedback(self, feedback):
		if isinstance(feedback, list):
			for fb in feedback:
				fb.__class__ = EntryFeedback
			self.feedback += feedback
		else:
			feedback.__class__ = EntryFeedback
			self.feedback.append(feedback)

	def add_episodes(self, episodes):
		if isinstance(episodes, list):
			for e in episodes:
				e.number = self.next_episode_number()
			self.episodes += episodes
		else:
			episodes.number = self.next_episode_number()
			self.episodes.append(episodes)

	def add_attachments(self, attachments):
		if isinstance(attachments, list):
			self.attachments += attachments
		else:
			self.attachments.append(attachments)

	'''
	list ids
	'''
	def episode_ids(self):
		return [episode.id for episode in self.episodes]


	def get_episode(self, episode_number):
		return filter(lambda episode: episode.number is episode_number, self.episodes)	

	'''
	Queries
	'''
	def competency_map(self):
		#need is_active check on career_episodes
		# comp_map = defaultdict(list)
		# for career_episode in self.career_episodes:
		# 	report_mappings = career_episode.competency_map()

		# 	for key, val in report_mappings.items():
		# 		comp_map[key] += val

		# return dict(comp_map)

		# TODO: remove? --> No longer using competency maps e.g. {competency_id: text_satisfying} ?
		pass

	def competency_set(self):
		comp_set = set()	#use set to manage the case where two reports exhibit same competency
		for career_episode in self.get_selected_episodes():
			comp_set = comp_set | career_episode.competency_set()

		return comp_set

	def is_industry(self):
		return self.entry_type == "Category 1"

	def is_cat234(self):
		return self.entry_type == "Category 2, 3 and 4"

	def is_coursework(self):
		return self.entry_type == "Coursework"

	'''
	Required Init helpers
	'''
	def next_episode_number(self):
		try:
			max_nums = max([e.number for e in self.episodes])
			return max_nums + 1
		except ValueError:
			return 1	# usually means max() 

	'''
	Other
	'''
	def get_selected_episodes(self):
		return [episode for episode in self.episodes if episode.selected]

	def is_active(self):
		return len(self.get_selected_episodes()) is not 0

	def __str__(self):
		return self.name

	def __repr__(self):
		return "<Entry %s>" % self.number

'''
Belongs to an Entry
Contains the career episode report + information relevant to report
'''
class CareerEpisode(Base):
	__tablename__		= "career_episode"
	#PK
	id 					= db.Column(db.Integer, primary_key=True, autoincrement=True)

	#FKs
	entry_id			= db.Column(db.Integer, db.ForeignKey('entry.id'))

	#data
	name                = db.Column(db.String(128))
	number  			= db.Column(db.Integer)
	report 				= db.Column(db.Text)
	selected			= db.Column(db.Boolean)	

	#relationships
	attachments 		= db.relationship('Attachment', secondary='career_episode_attachments', backref="career_episode")
	competencies 		= db.relationship('Competency', secondary='career_episode_competencies', backref="career_episode")
	feedback 			= db.relationship('CareerEpisodeFeedback', backref='episode')


	def __init__(self, *args, **kwargs):
		#data
		self.number 		= kwargs.pop("number", None)
		self.report 		= kwargs.pop("report", None)
		self.selected 		= kwargs.pop("selected", False)
		self.name           = kwargs.pop("name", None)

		#relationships
		self.feedback 		= kwargs.pop("feedback", [])
		self.attachments 	= kwargs.pop("attachments", [])
		self.competencies 	= kwargs.pop("competencies", [])

		if kwargs:
			raise TypeError("Invalid arguments: %s", kwargs.keys())

	def get_url(self):
		return url_for("eportfolio.episodes", version=1, student_id=self.entry.portfolio.id, entry_number=self.entry.number, episode_number=self.number)

	'''
	List extensions
	'''
	def add_feedback(self, feedback):
		if isinstance(feedback, list):
			for fb in feedback:
				fb.__class__ = CareerEpisodeFeedback
			self.feedback += feedback
		else:
			feedback.__class__ = CareerEpisodeFeedback
			self.feedback.append(feedback)

	def add_attachments(self, attachments):
		if isinstance(attachments, list):
			self.attachments += attachments
		else:
			self.attachments.append(attachments)

	def add_competencies(self, competencies):
		if isinstance(competencies, list):
			self.competencies += competencies
		else:
			self.competencies.append(competencies)

	'''
	Returns the mapping of competency to text in this report
	'''
	def build_competencies(self):
		#read self.report to build self's list of competencies
		pass

	'''
	Returns the set of competencies satisfied by this report
	'''
	def competency_set(self):
		return set(self.competencies)

	def __str__(self):
		return "Career Episode #%d" % self.number

	def __repr__(self):
		return "<CareerEpisode %d>" % self.number


class Attachment(Base):
	#pk
	id 			= db.Column(db.Integer, primary_key=True, autoincrement=True)
	student_id 	= db.Column(db.Integer, db.ForeignKey('portfolio.id'))
	
	#data
	file_name	= db.Column(db.String(512))
	file_url 	= db.Column(db.String(512))		#pointer to cloud storage for attachment file
	file_type 	= db.Column(db.Enum('Resume','Certificate of Work','Other'))

	#metadata
	file_size 	= db.Column(db.Integer)

	def __init__(self, file_type, file_name):

		self.file_type 	= file_type
		self.file_name  = file_name

	def get_url(self):
		return url_for("eportfolio.attachment", attachment_id=self.id, student_id=self.student_id, version=1)

'''
Feedback
'''
class Feedback(Base):
	#PK
	id 					= db.Column(db.Integer, primary_key=True, autoincrement=True)

	#data
	text 				= db.Column(db.Text)
	submitted 			= db.Column(db.Boolean)
	datetime_submitted 	= db.Column(db.DateTime)

	#relationships
	marker 				= db.relationship('Marker', uselist=False)
	
	#FK
	marker_id 			= db.Column(db.ForeignKey('marker.id'))

	#inheritance handling
	type 				= db.Column(db.String(64))
	__mapper_args__ 	= {'polymorphic_on': type}

	def __init__(self, marker, text, submitted=False):
		self.marker 	= marker
		self.text 		= text
		self.submitted 	= submitted

	def get_url(self):
		return url_for("eportfolio.feedback", feedback_id=self.id)

	def submit(self):
		self.submitted = True
		self.datetime_submitted = datetime.now()	#TODO: time zone handling required?

	'''
	Uses the polymorphic identity of the feedback
	object to infer which class's get_work_url() to call
	'''
	def get_work_url(self):
		#is this bad practice? (it's required for FeedbackSchema)
		if self.type == "portfolio_feedback":
			self.__class__ = PortfolioFeedback
			return self.get_work_url()

		elif self.type == "entry_feedback":
			print "changing feedback's class"
			self.__class__ = EntryFeedback
			return self.get_work_url()

		elif self.type == "episode_feedback":
			self.__class__ = CareerEpisodeFeedback
			return self.get_work_url()

		else:
			raise ValueError("Feedback (id=%d) not linked to any portfolio item" % self.id)

class PortfolioFeedback(Feedback):
	#PK
	id = db.Column(db.Integer, db.ForeignKey('feedback.id'), primary_key=True)
	
	#FK
	portfolio_id = db.Column(db.Integer, db.ForeignKey('portfolio.id'))

	__mapper_args__ = {
		'polymorphic_identity':'portfolio_feedback',
	}

	def get_work_url(self):
		return self.portfolio.get_url()

	def __init__(self, marker, text, submitted=False):
		super(PortfolioFeedback, self).__init__(marker, text, submitted)

class EntryFeedback(Feedback):
	# PK
	id = db.Column(db.Integer, db.ForeignKey('feedback.id'), primary_key=True)
	
	# FK
	entry_id = db.Column(db.Integer, db.ForeignKey('entry.id'))

	__mapper_args__ = {
		'polymorphic_identity':'entry_feedback',
	}

	def get_work_url(self):
		return self.entry.get_url()

	def __init__(self, marker, text, submitted=False):
		super(EntryFeedback, self).__init__(marker, text, submitted)


class CareerEpisodeFeedback(Feedback):
	id = db.Column(db.Integer, db.ForeignKey('feedback.id'), primary_key=True)

	# FK
	episode_id = db.Column(db.Integer, db.ForeignKey('career_episode.id'))

	__mapper_args__ = {
		'polymorphic_identity':'career_episode_feedback',
	}

	def get_work_url(self):
		return self.episode.get_url()

	def __init__(self, marker, text, submitted=False):
		super(CareerEpisodeFeedback, self).__init__(marker, text, submitted)

'''
EA Competencies

16 Competencies
'''
class Competency(Base):
	id 				= db.Column(db.Integer, primary_key=True, autoincrement=True)
	number 			= db.Column(db.String(12))
	name 			= db.Column(db.String(120))
	description 	= db.Column(db.Text)
	url 			= db.Column(db.String(256))

	def get_url(self):
		return url_for("eportfolio.competencies", version=1, competency_id=self.id)

	def __init__(self, name='team_work', number=None, url='teamwork.com', description=None):
		self.name 			= name
		self.number 		= number
		self.url 			= url
		self.description 	= description 