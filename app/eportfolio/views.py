# Import flask dependencies - whole bunch of useful stuff here btw.
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for, current_app, jsonify, make_response, logging
from sqlalchemy import exc

# Import the requirements for the LTI Integration
from pylti.flask import lti
from werkzeug.exceptions import NotFound
from werkzeug.utils import secure_filename

# Import the database object from the main app module
from app import db

# Import module models (i.e. User)
from app.eportfolio.models import *

# 
from app.eportfolio.schema import *

from datetime import datetime

from boto.s3.connection import S3Connection as AWS_S3_Connection
from boto.s3.key import Key as AWS_Key
import sys


from functools import wraps

# Define the blueprint: 'eportfolio', set its url prefix.
eportfolio = Blueprint('eportfolio', __name__, url_prefix='')

'''
VIEWS

(endpoints and methods) --> views
'''
(GET, POST, PUT, DELETE) = ('GET', 'POST', 'PUT', 'DELETE')

'''
Exceptions
'''

class InvalidUsage(Exception):
	status_code = 400

	def __init__(self, message, status_code=None, payload=None):
		Exception.__init__(self)
		self.message 		= message
		self.status_code 	= status_code
		self.payload 		= payload

	def to_dict(self):
		rv = {}
		rv['payload'] 		= self.payload
		rv['message'] 		= self.message
		rv['status_code'] 	= self.status_code
		return rv	

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
	response 				= jsonify(error.to_dict())
	response.status_code 	= error.status_code
	return response

@app.errorhandler(413) #File Size Too Large
def handle_file_size_too_large(error):
	return "", 413, None

@app.errorhandler(403)
def ltierror(exception=None):
	return str(exception), 403, None


'''
Decorators
'''
def check(item, state, decorator=None):
	if decorator is None:
		return lambda x: x
		
	return decorator if item is state else lambda x: x

def eportfolio_acl(f):
	if not app.config["TESTING"]:
		@wraps(f)
		def decorated_function(*args, **kwargs):
			print args
			print kwargs
			if kwargs["student_id"] != session["user_id"]:
				return "",403, None
			return f(*args, **kwargs)
		return decorated_function 
	else:
		return f


'''
LTI Authentication Endpoint

Blackboard LMS ---- POST ---> ePortfolio (/v1/lti/)
'''
@eportfolio.route('/v<version>/lti', methods=[POST])
@lti(request='initial', error=ltierror, app=current_app)
def lti_initial(version, lti=lti):
	
	app.logger.info("user_id: " 	+ str(lti.user_id))
	app.logger.info("email: " 		+ session["lis_person_contact_email_primary"])
	app.logger.info("roles: " 		+ str(lti.role))
	app.logger.info("fullname: "	+ session["lis_person_name_full"])
	app.logger.info("givenName: " 	+ session["lis_person_name_given"])
	app.logger.info("familyName: " 	+ session["lis_person_name_family"])

	user_id 	= lti.user_id
	first_name 	= session["lis_person_name_given"]
	last_name 	= session["lis_person_name_family"]
	user_email	= session["lis_person_contact_email_primary"]
	role 		= str(lti.role)
	
	if str(lti.role) == "Learner":
		#app.logger.info("Recognised role: Learner")

		try:
			student = Student.query.get_or_404( int(lti.user_id) )

			#app.logger.info("Student exists: " + str(student.id))

		except NotFound:
			student = Student(id=user_id, first_name=first_name, last_name=last_name)
			
			db.session.add(student)
			db.session.commit()

			#app.logger.info("Student created: " + str(student.id))

	elif str(lti.role) == "staff":
		pass

	else:
		app.logger.info("unrecognised role: %s", lti.role)

	response = make_response(redirect(app.config["EPORTFOLIO_FRONTEND_URL"] + str(lti.user_id)))
	
	response.set_cookie('user_id', 	user_id, domain=app.config["SESSION_COOKIE_DOMAIN"])
	response.set_cookie('first_name', first_name, domain=app.config["SESSION_COOKIE_DOMAIN"])
	response.set_cookie('last_name', last_name, domain=app.config["SESSION_COOKIE_DOMAIN"])
	response.set_cookie('user_email', user_email, domain=app.config["SESSION_COOKIE_DOMAIN"])
	response.set_cookie('role', role, domain=app.config["SESSION_COOKIE_DOMAIN"])


	return response

'''
Testing Setup
'''

@check(app.config["TESTING"], True)
@eportfolio.route('/test_setup', methods=[GET])
def create_students():
	db.drop_all()
	db.create_all()
	student1 = Student(id=534, first_name="Tim", last_name="Raphael")
	student2 = Student(id=123, first_name="Cal", last_name="B")
	student3 = Student(id=999, first_name="T", last_name="F")
	student4 = Student(id=20111134, first_name="K", last_name="H")
	student5 = Student(id=21100234, first_name="J", last_name="L")

	meg 	= Student(id="20506654", first_name="Megan", last_name="Crawley")
	tim 	= Student(id="21255093", first_name="Timothy", last_name="Raphael")
	callan 	= Student(id="20913856", first_name="Callan", last_name="Bright")
	cora 	= Student(id="21742778", first_name="Huiying", last_name="Hu")

	comp11 	= Competency(name='Comprehensive, theory based understanding', number="1.1", url='teamwork.com', description="Comprehensive, theory based understanding of the underpinning natural and physical sciences and the engineering fundamentals applicable to the engineering discipline.")
	comp12 	= Competency(name='Conceptual understanding', number="1.2", url='teamwork.com', description="Conceptual understanding of the, mathematics, numerical analysis, statistics, and computer and information sciences which underpin the engineering discipline.")
	comp13 	= Competency(name='In-depth understanding', number="1.3", url='teamwork.com', description="In-depth understanding of specialist bodies of knowledge within the engineering discipline.")
	comp14 	= Competency(name='Discernment', number="1.4", url='teamwork.com', description="Discernment of knowledge development and research directions within the engineering discipline.")


	db.session.add(comp11)
	db.session.add(comp12)
	db.session.add(comp13)
	db.session.add(comp14)

	db.session.add(meg)
	db.session.add(tim)
	db.session.add(callan)
	db.session.add(cora)

	db.session.add(student1)
	db.session.add(student2)
	db.session.add(student3)
	db.session.add(student4)
	db.session.add(student5)

	db.session.commit()
	return "Success"

'''
General CRUD Methods
'''
def get_objects(object_class, schema_class, ids=None):
	data = None

	if isinstance(ids, int) or isinstance(ids, long):
		_object = object_class.query.get_or_404(ids)
		data = schema_class().dump(_object).data
		
	elif isinstance(ids, list):
		objects = [object_class.query.get_or_404(_id) for _id in ids]		
		data = schema_class().dump(objects, many=True).data

	elif not ids:
		data = schema_class().dump(object_class.query.all(), many=True).data
	else:
		print type(ids)
		raise InvalidUsage('Invalid format for ID: ' + str(ids), status_code=400)

	return jsonify({ "data": data })


def update_objects(object_class, schema_class, ids=None):

	if isinstance(ids, int) or isinstance(ids, long):
		data, error = schema_class().load(request.get_json()["data"], instance=object_class.query.get_or_404(ids))

		if error:
				raise InvalidUsage('Invalid object data. ID: ' + str(ids), status_code=400, payload=error)

	elif ids is None:
		for o in request.get_json()["data"]:
			data, error = schema_class().load(o, instance=object_class.query.get_or_404(o["id"]))
			if error:
				raise InvalidUsage('Invalid multiple object data. ID: ' + str(o["id"]), status_code=400, payload=error)
	try:
		db.session.commit()
		return "", 202, None
	except exc.SQLAlchemyError as e:
		raise InvalidUsage('Database error: ' + str(e.__class__), status_code=400, payload=repr(e))


def delete_objects(object_class, schema_class, ids=None):
	
	if isinstance(ids, int) or isinstance(ids, long):
		# Single object
		obj = object_class.query.get_or_404(ids)

		try:
			db.session.delete(obj)
			db.session.commit()
			return "", 204, None
		except exc.SQLAlchemyError as e:
			InvalidUsage('Unable to delete objects. Error: ' + str(e.__class__), status_code=400)

	elif isinstance(ids, list):
		objs = [object_class.query.get_or_404(_id) for _id in ids]		

		try:
			for o in objs:
				db.session.delete(o)
			db.session.commit()
			return "", 204, None
		except exc.SQLAlchemyError as e:
				InvalidUsage('Unable to delete objects. Error: ' + str(e.__class__), status_code=400)

	elif ids is None:
		objs = object_class.query.all()

		try:
			for o in objs:
				db.session.delete(o)
			db.session.commit()
			return "", 204, None
		except exc.SQLAlchemyError as e:
				InvalidUsage('Unable to delete objects. Error: ' + str(e.__class__), status_code=400)


'''
File Handling Helpers
'''
def allowed_file(filename):
	# need to check file size
	return '.' in filename and \
		   filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


'''
ePortfolio
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>', methods=[GET, PUT])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def ePortfolio(version, student_id, lti=None):
	if request.method == GET:
		return get_objects(object_class=Portfolio, schema_class=PortfolioSchema, ids=int(student_id))

	elif request.method == PUT:
		return update_objects(object_class=Portfolio, schema_class=PortfolioSchema, ids=int(student_id))
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
ePortfolio - Export
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/export', methods=[GET])
def export(version, student_id):
	pass

'''
Attachments Collection
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/attachments', methods=[GET, POST, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def attachment_collection(version, student_id, lti=None):
	if request.method == GET:
		student = Student.query.get_or_404(student_id)
		return get_objects(object_class=Attachment, schema_class=AttachmentSchema, ids=student.portfolio.attachment_ids())
	
	elif request.method == POST:

		student = Student.query.get_or_404(student_id)

		attachment, errors = AttachmentSchema().load(request.get_json()["data"])

		if errors:
			raise InvalidUsage("Incorrect Data Format", status_code=404, payload=errors)
		try:
			print student
			print attachment
			student.portfolio.attachments.append(attachment)
			db.session.add(attachment)
			db.session.commit()

			return "",201,{"Location" : str(attachment.get_url())}

		except exc.SQLAlchemyError, e:
			InvalidUsage('Error: database error', status_code=400, payload=repr(e))

	elif request.method == DELETE:
		pass
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)



'''
Attachments
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/attachments/<attachment_id>', methods=[GET, POST, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def attachment(version, student_id, attachment_id, lti=None):
	if request.method == GET:
		return get_objects(object_class=Attachment, schema_class=AttachmentSchema, ids=int(attachment_id))
	elif request.method == POST:
		return update_objects(object_class=Attachment, schema_class=AttachmentSchema)
	elif request.method == PUT:
		## TODO: Upload an attachment
		# Gunna need some stuff here to suck up the and push it to AWS.
		# Then return the URL.
		file = request.files['file']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			
			# upload to an amazon bucket (dodgy i know, needs to be async)
			aws_con = AWS_S3_Connection(app.config['AWS_API_ACCESS_KEY'], app.config['AWS_API_SECRET_KEY'])
			if aws_con:
				bucket = aws_con.get_bucket(app.config['AWS_S3_BUCKET_NAME'])
				if bucket:

					attachment = Attachment.query.get_or_404(attachment_id)

					key = AWS_Key(bucket)
					key.key = hash(student_id + attachment_id)
					bytes_uploaded = key.set_contents_from_file(file,  headers={'Content-Disposition': 'attachment; filename=' + attachment.file_name})
					key.set_metadata('filename', attachment.file_name)

					attachment.file_url = key.generate_url(expires_in=0, query_auth=False)
					attachment.file_size = bytes_uploaded

					try:
						db.session.add(attachment)
						db.session.commit()
						return "", 201, None

					except exc.SQLAlchemyError:
						InvalidUsage('Error: database error', status_code=400)

		return "", 415, None # Unexcepted Content Type

	elif request.method == DELETE:
		return delete_objects(object_class=Attachment, schema_class=AttachmentSchema, ids=int(attachment_id))
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Entries Collection
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/entries', methods=[GET, POST, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def entries_collection(version, student_id, lti=None):
	if request.method == GET:
		entry_ids = Student.query.get_or_404(student_id).portfolio.entry_ids()
		return get_objects(object_class=Entry, schema_class=EntrySchema, ids=entry_ids)

	elif request.method == POST:
		student = Student.query.get_or_404(student_id)

		entry, errors = EntrySchema().load(request.get_json()["data"])

		if errors:
			raise InvalidUsage("Incorrect Data Format", status_code=400, payload=errors)
		try:
			student.portfolio.add_entries(entry)
			db.session.add(entry)
			db.session.commit()

			return "",201,{"Location" : str(entry.get_url())}
		except exc.SQLAlchemyError, e:
			InvalidUsage('Error: database error', status_code=400, payload=repr(e))
		
	elif request.method == PUT:
		return update_objects(object_class=Entry, schema_class=EntrySchema)
	elif request.method == DELETE:
		entry_ids = Student.query.get_or_404(student_id).portfolio.entry_ids()
		return delete_objects(object_class=Entry, schema_class=EntrySchema, ids=entry_ids)
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Entries
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/entries/<entry_number>', methods=[GET, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def entries(version, student_id, entry_number, lti=None):
	if request.method == GET:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		print type(entry.id), entry.id

		return get_objects(object_class=Entry, schema_class=EntrySchema, ids=entry.id)

	elif request.method == PUT:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()

		return update_objects(object_class=Entry, schema_class=EntrySchema, ids=entry.id)

	elif request.method == DELETE:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()

		return delete_objects(object_class=Entry, schema_class=EntrySchema, ids=entry.id)

	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Career Episode Reports Collection
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/entries/<entry_number>/episodes', methods=[GET, POST, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def episodes_collection(version, student_id, entry_number, lti=None):
	if request.method == GET:
		portfolio = Portfolio.query.get_or_404(student_id)
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		episode_ids = entry.episode_ids()

		return get_objects(CareerEpisode, CareerEpisodeSchema, ids=episode_ids)

	elif request.method == POST:
		
		student = Student.query.get_or_404(student_id)
		entry = Entry.query.filter_by(portfolio=student.portfolio, number=entry_number).first_or_404()

		episode, errors = CareerEpisodeSchema().load(request.get_json()["data"])

		if errors:
			raise InvalidUsage("Incorrect Data Format", status_code=404, payload=errors)
		try:
			entry.add_episodes(episode)
			db.session.add(episode)
			db.session.commit()

			return "",201,{"Location" : str(episode.get_url())}

		except exc.SQLAlchemyError:
			InvalidUsage('Error: database error', status_code=400, payload=repr(e))

	elif request.method == PUT:
		return update_objects(CareerEpisode, CareerEpisodeSchema)
	elif request.method == DELETE:
		portfolio = Portfolio.query.get_or_404(student_id)
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		episode_ids = entry.episode_ids()

		return delete_objects(CareerEpisode, CareerEpisodeSchema, ids=episode_ids)
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Career Episode Reports
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/entries/<entry_number>/episodes/<episode_number>', methods=[GET, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def episodes(version, student_id, entry_number, episode_number, lti=None):
	if request.method == GET:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		episode = CareerEpisode.query.filter_by(entry=entry, number=episode_number).first_or_404()

		return get_objects(object_class=CareerEpisode, schema_class=CareerEpisodeSchema, ids=episode.id)

	elif request.method == PUT:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		episode = CareerEpisode.query.filter_by(entry=entry, number=episode_number).first_or_404()
		return update_objects(object_class=CareerEpisode, schema_class=CareerEpisodeSchema, ids=episode.id)

	elif request.method == DELETE:
		portfolio = Student.query.get_or_404(student_id).portfolio
		entry = Entry.query.filter_by(portfolio=portfolio, number=entry_number).first_or_404()
		episode = CareerEpisode.query.filter_by(entry=entry, number=episode_number).first_or_404()

		return delete_objects(object_class=CareerEpisode, schema_class=CareerEpisodeSchema, ids=episode.id)
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Feedback Collection
'''
@eportfolio.route('/v<version>/eportfolios/<student_id>/feedback', methods=[GET, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def feedback_collection(version, student_id, lti=None):
	# I know I can't english, got any better ideas for this? - Tim
	if request.method == GET:
		return FeedbackSchema().jsonify(Feedback.query.filter_by(student_id=student_id), many=True)
	elif request.method == POST:
		pass
		#integrate the following
		'''
		request.get_json()["data"] --> to get the item object

		then:

		if type(item) is Portfolio:
			feedback = PortfolioFeedback(student_id=item.student_id, marker=self, text=text)
			portfolio = Portfolio.query.get_or_404(item.student_id)
			portfolio.feedback.append(feedback)

		elif type(item) is Entry:
			feedback = EntryFeedback(student_id=item.student_id, marker=self, text=text)
			entry = Entry.query.get_or_404(item.id)
			entry.feedback.append(feedback)

		elif type(item) is CareerEpisode:
			feedback = CareerEpisodeFeedback(student_id=item.entry.student_id, marker=self, text=text)
			episode = CareerEpisode.query.get_or_404(item.id)
			episde.feedback.append(feedback)
		'''
	elif request.method == DELETE:
		pass
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Feedback
'''
@eportfolio.route('/v<version>/feedback/<feedback_id>', methods=[GET, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
@eportfolio_acl
def feedback(version, feedback_id, lti=None):
	if request.method == GET:
		return FeedbackSchema().jsonify(Feedback.query.get_or_404(feedback_id))
	elif request.method == PUT:
		pass
	elif request.method == DELETE:
		pass
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Competencies
'''
@eportfolio.route('/v<version>/competencies', methods=[GET, POST, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
def competencies_collection(version, lti=None):
	comp_ids = [int(c.id) for c in Competency.query.all()]

	if request.method == GET:
		return get_objects(object_class=Competency, schema_class=CompetencySchema)
	elif request.method == POST:
		newComp, errors = CompetencySchema().load(request.get_json()["data"])
		if errors:
			raise InvalidUsage("Incorrect Data Format", status_code=400, payload=errors)
		try:
			db.session.add(newComp)
			db.session.commit()

			return "",201,{"Location" : str(newComp.get_url())}
		except exc.SQLAlchemyError, e:
			InvalidUsage('Error: database error', status_code=400, payload=repr(e))

	elif request.method == PUT:
		return update_objects(object_class=Competency, schema_class=CompetencySchema, ids=comp_ids)

	elif request.method == DELETE:
		return delete_objects(object_class=Competency, schema_class=CompetencySchema)

	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Competencies
'''
@eportfolio.route('/v<version>/competencies/<competency_id>', methods=[GET, PUT, DELETE])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
def competencies(version, competency_id, lti=None):
	if request.method == GET:
		return get_objects(object_class=Competency, schema_class=CompetencySchema, ids=int(competency_id))
	elif request.method == PUT:
		return update_objects(object_class=Competency, schema_class=CompetencySchema, ids=int(competency_id))
			
	elif request.method == DELETE:
		return delete_objects(object_class=Competency, schema_class=CompetencySchema, ids=int(competency_id))

	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Markers Collection
'''
@eportfolio.route('/v<version>/markers', methods=[GET, POST, PUT, DELETE])
def markers(version, lti=None):
	if request.method == GET:
		
		return MarkerSchema().jsonify(Marker.query.all(), many=True)

	elif request.method == POST:
		pass
	elif request.method == PUT:
		pass
	elif request.method == DELETE:
		pass
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Markers
'''
@eportfolio.route('/v<version>/markers/<marker_id>', methods=[GET, PUT, DELETE])
#@eportfolio_staff_acl
def marker(version, marker_id, lti=None):
	if request.method == GET:
		return MarkerSchema().jsonify(Marker.query.get_or_404(marker_id))
	elif request.method == PUT:
		pass
	elif request.method == DELETE:
		pass
	else:
		raise InvalidUsage('Unimplemented Method', status_code=400)

'''
Types - File
'''
@eportfolio.route('/v<version>/types/file', methods=[GET])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
def file_types(version, lti=None):
	if not request.method == GET:
		raise InvalidUsage('Unimplemented Method', status_code=400)

	types = [{"fileType": "Resume"}, {"fileType":"Certificate of Work"}, {"fileType":"Other"}]

	return jsonify(types)

'''
Types - Entry
'''
@eportfolio.route('/v<version>/types/entry', methods=[GET])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
def entry_types(version, lti=None):
	if not request.method == GET:
		raise InvalidUsage('Unimplemented Method', status_code=400)

	types = [
				{"entryType": "Category 1", "categories": [1]},
				{"entryType": "Category 2, 3 and 4", "categories": [2,3,4]}, 
				{"entryType": "Coursework", "categories": []}
			]

	return jsonify(types)


@eportfolio.route('/v<version>/files', methods=[GET])
@check(app.config["TESTING"], False, decorator=lti(request='session', error=ltierror, role="student", app=current_app))
def files(version, lti=None):
	if not request.method == GET:
		raise InvalidUsage('Unimplemented Method', status_code=400)

	files = [
				{"fileType" : "EngineersAustraliaCompetencies", "fileURL" : url_for('static', filename="EAStage1Competencies.pdf")},
				{"fileType" : "HelpPages", "fileURL" : url_for('static', filename="HelpPages.pdf")}
			]

	return jsonify(files)




