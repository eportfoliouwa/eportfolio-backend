from app import ma
from inflection import camelize, underscore
from marshmallow import post_dump, pre_load, post_load

from collections import namedtuple

from app.eportfolio.models import *

'''
fe (frontend) 	= Property key of json to be seen on the front end
be (backend) 	= Assoc. attribute name in the database 
'''
RenamePair = namedtuple("RenamePair", "be fe")


class BaseSchema(ma.ModelSchema):

	def __init__(self, *args, **kwargs):
		self.rename_pairs = kwargs.pop("rename_pairs", [])
		super(BaseSchema, self).__init__(*args, **kwargs)

	@post_dump(pass_many=True)
	def post_dump_process(self, json, many):
		self.rename_fields(json)
		self.to_camel(json)

	@pre_load(pass_many=True)
	def pre_load_process(self, json, many):
		self.rename_fields(json, back_to_front=False)
		self.to_snake(json)

	'''
	toCamel

	variable_name --> variableName
	'''
	def to_camel(self, json):
		if type(json) is list:
			self.to_camel_list(json)
		elif type(json) is dict:
			self.to_camel_single(json)
		else:
			raise TypeError("to_camel can not be performed on type: %s" % str(type(json)))	

	def to_camel_single(self, json):
		for key, value in json.items():
			data = json[key]
			del json[key]
			json[camelize(key, uppercase_first_letter=False)] = data

	def to_camel_list(self, json):
		for item in json:
			self.to_camel(item)

	'''
	self.to_snake

	variableName --> variable_name
	'''
	def to_snake(self, json):
		if type(json) is list:
			self.to_snake_list(json)
		elif type(json) is dict:
			self.to_snake_single(json)
		else:
			raise TypeError("to_snake can not be performed on type: %s" % str(type(json)))	

	def to_snake_single(self, json):
		for key, value in json.items():
			data = json[key]
			del json[key]
			json[underscore(key)] = data

	def to_snake_list(self, json):
		for item in json:
			self.to_snake(item)

	'''
	renaming fields

	old_name --> new_name pairs
	'''
	def rename_fields(self, json, back_to_front=True):
		rename_dict = self.build_rename_dict(back_to_front)

		if not rename_dict:
			return

		if type(json) is list:
			self.rename_fields_list(json, back_to_front)

		elif type(json) is dict:
			self.rename_fields_single(json, rename_dict)

		else:
			raise TypeError("rename_fields can not be performed on type: %s" % str(type(json)))

	def build_rename_dict(self, back_to_front):
		if back_to_front:
			rename_dict = {pair.be: pair.fe for pair in self.rename_pairs}
		else:
			rename_dict = {pair.fe: pair.be for pair in self.rename_pairs}

		return rename_dict

	def rename_fields_single(self, json, rename_dict):
		for old_name, new_name in rename_dict.items():
			try:
				json[new_name] = json[old_name]
				del json[old_name]
			
			except KeyError, ke:
				pass

	def rename_fields_list(self, json, back_to_front):
		for item in json:
			self.rename_fields(item, back_to_front)

## Attachment (object)
class AttachmentSchema(BaseSchema):

	class Meta:
		model = Attachment
		exclude = ('date_modified', 'portfolio', 'entry', 'career_episode')

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='attachmentFileType', be='file_type'),
							RenamePair(fe='attachmentFileURL', be='file_url'),
							RenamePair(fe='attachmentFileSize', be='file_size'),
							RenamePair(fe='attachmentFileName', be='file_name')
						])
		super(AttachmentSchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

## User (object)
class UserSchema(BaseSchema):
	class Meta:
		model = User
		fields = ('id', 'first_name', 'last_name', 'user_email', 'date_created', 'date_modified')

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='userId', be='id'),
							RenamePair(fe='userFirstName', be='first_name'),
							RenamePair(fe='userLastName', be='last_name'),
							RenamePair(fe='userEmail', be='user_email')
						])

		super(UserSchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

## Student (object)
class StudentSchema(UserSchema):

	class Meta(UserSchema.Meta):
		model = Student

	def __init__(self, *args, **kwargs):
		super(StudentSchema, self).__init__(*args, **kwargs)

## Marker (object)
class MarkerSchema(UserSchema):
	class Meta(UserSchema.Meta):
		fields 	= UserSchema.Meta.fields
		model 	= Marker
		fields 	+= ('students',)

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='markerStudents', be='students')
						])
		
		super(MarkerSchema, self).__init__(*args, **kwargs)

	#relationships
	students = ma.Nested(StudentSchema, many=True)

## UnitAdmin (object)
#TODO: implement

## Competency (object)
class CompetencySchema(BaseSchema):
	class Meta:
		model = Competency
		exclude = ('date_created', 'date_modified', 'career_episode')

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='competencyName', be='name'),
							RenamePair(fe='competencyDescription', be='description'),
							RenamePair(fe='competencyNumber', be='number'),
							RenamePair(fe='competencyUrl', be='url')
						])
		super(CompetencySchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

## Feedback (object)
class FeedbackSchema(BaseSchema):
	class Meta:
		model = Feedback
		exclude = ('type',)

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(be="marker", fe="feedbackMarker"),
							RenamePair(be="submitted", fe="feedbackSubmitted"),
							RenamePair(be="text", fe="feedbackComments"),
							RenamePair(be="datetime_submitted", fe="feedbackSubmittedDate"),
							RenamePair(be="work_url", fe="feedbackWorkUrl"),
						])
		
		super(FeedbackSchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

	#relationships
	marker 		= ma.Nested(MarkerSchema)

	#functions
	work_url 	= ma.Function(lambda fb: fb.get_work_url())

## CareerEpisode (object)
class CareerEpisodeSchema(BaseSchema):
	class Meta:
		model = CareerEpisode

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='episodeNumber', be='number'),
							RenamePair(fe='episodeName', be='name'),
							RenamePair(fe='episodeAttachments', be='attachments'),
							RenamePair(fe='episodeCompetencies', be='competencies'),
							RenamePair(fe='episodeReport', be='report'),
							RenamePair(fe='episodeFeedback', be='feedback'),
							RenamePair(fe='episodeSelected', be='selected'),
							RenamePair(fe='episodeEntry', be='entry')
						])
		super(CareerEpisodeSchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

	#relationships
	feedback 		= ma.Nested(FeedbackSchema, 		many=True)
	competencies 	= ma.Nested(CompetencySchema, 		many=True)
	attachments 	= ma.Nested(AttachmentSchema,	    many=True)

## Entry (object)
class EntrySchema(BaseSchema):
	class Meta:
		model = Entry
		exclude = ('portfolio',)
		
	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(be="name", 				fe="entryName"),
							RenamePair(be="number", 			fe="entryNumber"),
							RenamePair(be='start_date', 		fe='entryStartDate'),
							RenamePair(be='end_date',	 		fe='entryEndDate'),
							RenamePair(be='description', 		fe='entryDescription'),
							RenamePair(be='description', 		fe='entryDescription'),
							RenamePair(be='entry_type', 		fe='entryType'),
							RenamePair(be='episodes', 			fe='entryCareerEpisodes'),
							RenamePair(be='hours_accrued', 		fe='entryHoursAccrued'),
							RenamePair(be='supervisor_name', 	fe='entrySupervisorName'),
							RenamePair(be='supervisor_position',fe='entrySupervisorPosition'),
							RenamePair(be='attachments', 		fe='entryAttachments'),
							RenamePair(be='feedback', 			fe='entryFeedback'),
						])

		super(EntrySchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)

	#relationships
	feedback 	= ma.Nested(FeedbackSchema, 		many=True)
	episodes 	= ma.Nested(CareerEpisodeSchema, 	many=True)
	attachments = ma.Nested(AttachmentSchema, 	    many=True)

## Portfolio (object)
class PortfolioSchema(BaseSchema):
	class Meta:
		model 	= Portfolio

	def __init__(self, *args, **kwargs):
		rename_pairs = 	kwargs.pop("rename_pairs", [
							RenamePair(fe='portfolioStudent', be='student'),
							RenamePair(fe='portfolioStatus', be='status'),
							RenamePair(fe='portfolioEntries', be='entries'),
							RenamePair(fe='portfolioSubmissionStatus', be='status'),
							RenamePair(fe='portfolioFeedback', be='feedback'),
							RenamePair(fe='portfolioAttachments', be='attachments'),
							RenamePair(fe='portfolioCat1HoursCompleted', be='cat_1_hours_completed'),
							RenamePair(fe='portfolioCat234HoursCompleted', be='cat_234_hours_completed'),
							RenamePair(fe='portfolioHoursCompleted', be='hours_completed'),
							RenamePair(fe='portfolioCat1HoursCompletedPercent', be='cat_1_hours_completed_percent'),
							RenamePair(fe='portfolioHoursCompletedPercent', be='hours_completed_percent'),
							RenamePair(fe='portfolioCompetenciesCompleted', be='competencies_completed'),
							RenamePair(fe='portfolioCompetenciesCompletedPercent', be='competencies_completed_percent'),
							RenamePair(fe='portfolioValidForSubmission', be='valid_for_submission'),
							RenamePair(fe='portfolioResumeUploaded', be='resume_uploaded'),
							RenamePair(fe='portfolioSubmitted', be='submitted'),
							RenamePair(fe='portfolioSubmittionDate', be='submission_date'),
							RenamePair(fe='portfolioMarked', be='marked'),
							RenamePair(fe='portfolioMark', be='mark')
						])
		super(PortfolioSchema, self).__init__(rename_pairs=rename_pairs, *args, **kwargs)


	#relationships
	student 				= ma.Nested(StudentSchema)
	entries 				= ma.Nested(EntrySchema, 			many=True)
	feedback 				= ma.Nested(FeedbackSchema, 		many=True)
	attachments 			= ma.Nested(AttachmentSchema,	many=True)

	#functions
	cat_1_hours_completed 			= ma.Function(lambda pf: pf.hours_completed_cat_1())
	cat_234_hours_completed 		= ma.Function(lambda pf: pf.hours_completed_cat_234())
	hours_completed 				= ma.Function(lambda pf: pf.hours_completed_overall())
	cat_1_hours_completed_percent	= ma.Function(lambda pf: pf.hours_completed_cat_1_percent())
	hours_completed_percent 		= ma.Function(lambda pf: pf.hours_completed_percent_overall())
	competencies_completed 			= ma.Function(lambda pf: pf.competencies_completed())
	competencies_completed_percent 	= ma.Function(lambda pf: pf.competencies_completed_percent())
	valid_for_submission 			= ma.Function(lambda pf: pf.valid_for_submission())
	resume_uploaded 				= ma.Function(lambda pf: pf.resume_uploaded())
	marked 							= ma.Function(lambda pf: pf.is_marked())
	submitted						= ma.Function(lambda pf: pf.is_submitted())


class PortfolioStatusSchema(PortfolioSchema):
	class Meta(PortfolioSchema.Meta):
		fields = ('hours_completed_percent', 'hours_completed',
			'competencies_completed_percent', 'competencies_completed',
			'resume_uploaded', 'valid_for_submission', 'marked', 'mark')

	def __init__(self, *args, **kwargs):
		super(PortfolioStatusSchema, self).__init__(*args, **kwargs)

## CareerEpisodeBrief (object)
class CareerEpisodeBriefSchema(CareerEpisodeSchema):
	class Meta(CareerEpisodeSchema.Meta):
		model = CareerEpisode
		exclude = ('report', 'feedback')

	def __init__(self, *args, **kwargs):
		super(CareerEpisodeBriefSchema, self).__init__(*args, **kwargs)

class CareerEpisodeFeedbackSchema(FeedbackSchema):
	class Meta(CareerEpisodeSchema.Meta):
		fields = ('id', 'feedback')

	def __init__(self, *args, **kwargs):
		super(CareerEpisodeFeedbackSchema, self).__init__(*args, **kwargs)

	#relationships
	feedback = ma.Nested(FeedbackSchema, 	many=True)

class EntryFeedbackSchema(FeedbackSchema):
	class Meta(EntrySchema.Meta):
		fields = ('id', 'feedback', 'episodes')

	def __init__(self, *args, **kwargs):		
		super(CareerEpisodeFeedbackSchema, self).__init__(*args, **kwargs)
		
	#relationships
	feedback = ma.Nested(FeedbackSchema, 				many=True)
	episodes = ma.Nested(CareerEpisodeFeedbackSchema, 	many=True)
